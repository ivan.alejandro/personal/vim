# Installation
```bash
cp -rvp vim ~/.vim
cp -rvp vimrc ~/.vimrc
git submodule update --init --recursive
apt install build-essential cmake python3-dev
python3 ~/.vim/plugged/YouCompleteMe/install.py --all
```
